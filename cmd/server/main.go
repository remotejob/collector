package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/remotejob/collector/internal/config"
	"gitlab.com/remotejob/collector/pkg/handler"
)

var configuration *config.Config
var err error

func Routes(configuration *config.Config) *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", handler.New(configuration).Alive)
	router.HandleFunc("/v0/insert", handler.New(configuration).Insert)
	router.HandleFunc("/v0/getall", handler.New(configuration).Getall)

	return router
}

func main() {

	configuration, err = config.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}

	router := Routes(configuration)
	log.Fatal(http.ListenAndServe(":8080", router))
}
