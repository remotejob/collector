
go build -o bin/server gitlab.com/remotejob/collector/cmd/server && \
scp bin/server root@138.197.103.130:servernew && \
scp -r templates  root@138.197.103.130: && \
ssh root@138.197.103.130 systemctl stop collector.service && \
ssh root@138.197.103.130 cp servernew server && \
# scp collector.config.toml root@138.197.103.130:
ssh root@138.197.103.130 systemctl start collector.service

journalctl -f -u collector