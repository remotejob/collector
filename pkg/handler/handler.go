package handler

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"sort"
	"strings"
	"time"

	"github.com/golang/gddo/httputil/header"
	"gitlab.com/remotejob/collector/internal/config"
	"gitlab.com/remotejob/collector/internal/domains"
	"gitlab.com/remotejob/collector/pkg/pogrebhandler"
)

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Getall(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":

		tmpl, err := template.ParseFiles("templates/index.html")
		if err != nil {
			log.Println(err)

		}

		_, vs, err := pogrebhandler.GetAll(config.Config)
		if err != nil {
			log.Fatalln(err)

		}

		var res domains.Stat
		uniqcl := make(map[string]struct{}, 0)

		for _, v := range vs {

			var insertData domains.InsertData
			vstr := string(v)

			err := json.Unmarshal([]byte(vstr), &insertData)
			if err != nil {
				log.Println(err)

			} else {

				uniqcl[insertData.Guuid] = struct{}{}
				res.Records = append(res.Records, insertData)
			}
		}


		sort.Slice(res.Records, func(i, j int) bool { return res.Records[i].Date.After(res.Records[j].Date) })

		res.UniqClient = len(uniqcl)

		tmpl.Execute(w, res)

	default:

		fmt.Fprintf(w, "GET methods are supported.")
	}

}

func (config *Config) Alive(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Alive", "OK")

	w.Write([]byte("OK"))
}
func (config *Config) Insert(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()

	switch r.Method {

	case "POST":

		var payload domains.Payload
		if r.Header.Get("Content-Type") != "" {
			value, _ := header.ParseValueAndParams(r.Header, "Content-Type")
			if value != "application/json" {
				msg := "Content-Type header is not application/json"
				http.Error(w, msg, http.StatusUnsupportedMediaType)
				return
			}
		}

		// r.Body = http.MaxBytesReader(w, r.Body, 1048576)
		err := json.NewDecoder(r.Body).Decode(&payload)
		if err != nil {

			log.Println("POST", err.Error())

		}
		// dec.DisallowUnknownFields()
		// err := dec.Decode(&payload)
		// if err != nil {
		// 	var syntaxError *json.SyntaxError
		// 	var unmarshalTypeError *json.UnmarshalTypeError

		// 	switch {
		// 	// Catch any syntax errors in the JSON and send an error message
		// 	// which interpolates the location of the problem to make it
		// 	// easier for the client to fix.
		// 	case errors.As(err, &syntaxError):
		// 		msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
		// 		http.Error(w, msg, http.StatusBadRequest)

		// 	// In some circumstances Decode() may also return an
		// 	// io.ErrUnexpectedEOF error for syntax errors in the JSON. There
		// 	// is an open issue regarding this at
		// 	// https://github.com/golang/go/issues/25956.
		// 	case errors.Is(err, io.ErrUnexpectedEOF):
		// 		msg := fmt.Sprintf("Request body contains badly-formed JSON")
		// 		http.Error(w, msg, http.StatusBadRequest)

		// 	// Catch any type errors, like trying to assign a string in the
		// 	// JSON request body to a int field in our Person struct. We can
		// 	// interpolate the relevant field name and position into the error
		// 	// message to make it easier for the client to fix.
		// 	case errors.As(err, &unmarshalTypeError):
		// 		msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset)
		// 		http.Error(w, msg, http.StatusBadRequest)

		// 	// Catch the error caused by extra unexpected fields in the request
		// 	// body. We extract the field name from the error message and
		// 	// interpolate it in our custom error message. There is an open
		// 	// issue at https://github.com/golang/go/issues/29035 regarding
		// 	// turning this into a sentinel error.
		// 	case strings.HasPrefix(err.Error(), "json: unknown field "):
		// 		fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
		// 		msg := fmt.Sprintf("Request body contains unknown field %s", fieldName)
		// 		http.Error(w, msg, http.StatusBadRequest)

		// 	// An io.EOF error is returned by Decode() if the request body is
		// 	// empty.
		// 	case errors.Is(err, io.EOF):
		// 		msg := "Request body must not be empty"
		// 		http.Error(w, msg, http.StatusBadRequest)

		// 	// Catch the error caused by the request body being too large. Again
		// 	// there is an open issue regarding turning this into a sentinel
		// 	// error at https://github.com/golang/go/issues/30715.
		// 	case err.Error() == "http: request body too large":
		// 		msg := "Request body must not be larger than 1MB"
		// 		http.Error(w, msg, http.StatusRequestEntityTooLarge)

		// 	// Otherwise default to logging the error and sending a 500 Internal
		// 	// Server Error response.
		// 	default:
		// 		log.Println(err.Error())
		// 		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		// 	}
		// 	return
		// }
		// if dec.More() {
		// 	msg := "Request body must only contain a single JSON object"
		// 	http.Error(w, msg, http.StatusBadRequest)
		// 	return
		// }

		if payload.Site != "" && payload.Mob != "" {
			now := time.Now()
			datatoInsert := domains.InsertData{now, strings.TrimSpace(payload.Site), strings.TrimSpace(payload.Guuid), strings.TrimSpace(payload.Mob), strings.TrimSpace(payload.Ua),
				strings.TrimSpace(payload.Urlp), strings.TrimSpace(payload.Referer)}

			config.Queue.Enqueue(datatoInsert)

			log.Println("Queue", config.Queue.GetLen())

		}

		quesize := config.Queue.GetLen()
		if quesize > 2 {
			var allhits []domains.InsertData

			for i := 0; i < quesize; i++ {
				item, err := config.Queue.Dequeue()
				if err != nil {
					log.Println(err)

				} else {

					allhits = append(allhits, item.(domains.InsertData))

				}

			}
			// badgerhandler.InsertAllQue(config.Config, allhits)
			pogrebhandler.InsertAllQue(config.Config, allhits)

		}

	default:

		fmt.Fprintf(w, "Sorry,and POST methods are supported.")
	}

}
