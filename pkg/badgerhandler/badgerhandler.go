package badgerhandler

import (
	"bytes"
	"encoding/json"
	"log"

	"github.com/dgraph-io/badger"
	"gitlab.com/remotejob/collector/internal/common"
	"gitlab.com/remotejob/collector/internal/config"
	"gitlab.com/remotejob/collector/internal/domains"
)

func InsertAllQue(conf *config.Config, allque []domains.InsertData) error {

	wb := conf.BadgerDB.NewWriteBatch()
	defer wb.Cancel()

	for _, item := range allque {

		uuid, err := common.Buulid()
		if err != nil {
			log.Fatalln(err)
		}

		byteData := new(bytes.Buffer)

		json.NewEncoder(byteData).Encode(item)

		err = wb.Set(uuid, byteData.Bytes())
		if err != nil {
			return err
		}

	}
	err := wb.Flush()
	if err != nil {
		return err
	}

	return nil
}

func Insert(conf *config.Config, uuid []byte, page []byte) error {
	txn := conf.BadgerDB.NewTransaction(true)
	if err := txn.Set(uuid, page); err == badger.ErrTxnTooBig {
		if err := txn.Commit(); err != nil {
			return err
		}
		txn = conf.BadgerDB.NewTransaction(true)
		if err := txn.Set(uuid, page); err != nil {

			return err
		}
	}
	if err := txn.Commit(); err != nil {

		return err
	}

	return nil

}

func InsertPage(conf *config.Config, txn *badger.Txn, uuid []byte, page []byte) {

	if err := txn.Set(uuid, page); err == badger.ErrTxnTooBig {
		if err := txn.Commit(); err != nil {
			panic(err)
		}
		txn = conf.BadgerDB.NewTransaction(true)
		if err := txn.Set(uuid, page); err != nil {
			panic(err)
		}
	}

}

func DeletePages(conf *config.Config, keys [][]byte) error {

	txn := conf.BadgerDB.NewTransaction(true)

	for _, k := range keys {

		if err := txn.Delete(k); err == badger.ErrTxnTooBig {
			if err := txn.Commit(); err != nil {
				panic(err)
			}
			txn = conf.BadgerDB.NewTransaction(true)
			if err := txn.Delete(k); err != nil {
				panic(err)
			}
		}

	}

	if err := txn.Commit(); err != nil {
		panic(err)
	}

	return nil

}

func CountPages(conf *config.Config) (int, error) {

	var res int

	err := conf.BadgerDB.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {

			res = res + 1

		}
		return nil
	})

	return res, err

}

func GetPages(conf *config.Config) ([][]byte, [][]byte, error) {

	var key [][]byte
	var val [][]byte

	err := conf.BadgerDB.View(func(txn *badger.Txn) error {
		// var count int
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			err := item.Value(func(v []byte) error {
				key = append(key, k)
				val = append(val, v)
				return nil
			})
			if err != nil {
				return err
			}
		}

		return nil
	})

	return key, val, err

}
