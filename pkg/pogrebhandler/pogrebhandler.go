package pogrebhandler

import (
	"bytes"
	"encoding/json"
	"log"

	"github.com/akrylysov/pogreb"
	"gitlab.com/remotejob/collector/internal/common"
	"gitlab.com/remotejob/collector/internal/config"
	"gitlab.com/remotejob/collector/internal/domains"
)

func InsertAllQue(conf *config.Config, allque []domains.InsertData) error {

	for _, item := range allque {

		uuid, err := common.Buulid()
		if err != nil {
			log.Fatalln(err)
		}

		byteData := new(bytes.Buffer)

		json.NewEncoder(byteData).Encode(item)

		err = conf.PogrebDB.Put(uuid, byteData.Bytes())
		if err != nil {
			// log.Fatal(err)
			return err
		}

	}

	return nil

}
func GetAll(conf *config.Config) ([][]byte, [][]byte, error) {

	var key [][]byte
	var val [][]byte
	it := conf.PogrebDB.Items()
	for {
		k, v, err := it.Next()
		if err != nil {
			if err != pogreb.ErrIterationDone {
				log.Fatal(err)
			}
			break
		}
		key = append(key, k)
		val = append(val, v)
		// log.Printf("%s %s", key, val)
	}

	return key, val, nil

}
