package common

import (

	"math/rand"
	"time"

	"github.com/oklog/ulid/v2"
)

func Random(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(max-min) + min
}

func Buulid() ([]byte, error) {

	var res []byte
	t := time.Now()
	rand.Seed(time.Now().UTC().UnixNano())
	entropy := ulid.Monotonic(rand.New(rand.NewSource(t.UnixNano())), 0)

	uuid, err := ulid.New(ulid.Timestamp(t), entropy)
	if err != nil {
		return nil,err 
	}
	
	uuidbin, err := uuid.MarshalText()
	if err != nil {
		return nil, err
	}


	res = uuidbin

	return res, nil


}
