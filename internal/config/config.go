package config

import (
	"fmt"
	"log"

	"github.com/akrylysov/pogreb"
	badger "github.com/dgraph-io/badger"
	"github.com/enriquebris/goconcurrentqueue"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

type Constants struct {
	Badger struct {
		Url string
	}
	Pogreb struct {
		Url string
	}
}

type Config struct {
	Constants
	BadgerDB *badger.DB
	PogrebDB *pogreb.DB
	Queue    *goconcurrentqueue.FIFO
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}
	dbb, err := badger.Open(badger.DefaultOptions(config.Badger.Url))
	if err != nil {
		log.Fatal(err)
	}
	config.BadgerDB = dbb
	dbp, err := pogreb.Open(config.Pogreb.Url,nil)
	if err != nil {
		log.Fatal(err)
	}
	config.PogrebDB = dbp


	config.Queue = goconcurrentqueue.NewFIFO()

	return &config, err
}

func initViper() (Constants, error) {
	viper.SetConfigName("collector.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")                // Search the root directory for the configuration file
	err := viper.ReadInConfig()             // Find and read the config file
	if err != nil {                         // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
