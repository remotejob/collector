package domains

import "time"

type Payload struct {
	Site    string `json: "Site"`
	Guuid   string `json: "Guuid"`
	Mob     string `json: "Mob"`
	Ua      string `json: "Ua"`
	Urlp    string `json: "Urlp"`
	Referer string `json: "Referer"`
}

type InsertData struct {
	Date    time.Time `json: "Date"`
	Site    string    `json: "Site"`
	Guuid   string    `json: "Guuid"`
	Mob     string    `json: "Mob"`
	Ua      string    `json: "Ua"`
	Urlp    string    `json: "Urlp"`
	Referer string    `json: "Referer"`
}

type Stat struct {
	UniqClient int
	Records    []InsertData
}


// func sort.Slice(dateSlice, func(i, j int) bool {
//     return dateSlice[i].sortByThis.Before(dateSlice[j].sortByThis)
// })

// type SortedByDateRec []InsertData

// func (a SortedByDateRec) Len() int {
//     return len(a)
// }

// func (a SortedByDateRec) Less(i, j int) bool {
//     return a[i].sortByThis.Before(a[j].sortByThis)
// }

// func (a SortedByDateRec) Swap(i, j int) {
//     a[i], a[j] = a[j], a[i]
// }
